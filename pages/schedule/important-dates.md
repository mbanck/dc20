---
name: Important Dates
---
<style type="text/css">
tr td:first-child {
  width: 10rem;
}
</style>



| **APRIL**             |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| ?? (------)           | Opening of attendee registration and requesting bursaries; Opening of Call For Proposals |


| **MAY**               |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| ?? (------)           | Last day to submit bursary applications                              |
| ?? (------)           | Bursary decisions posted                                             |


| **JUNE**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| ?? (------)           | Last day for submitting a talk that will be considered for the official schedule             |
| ?? (------)           | Last day to accept or withdraw travel bursary (first round)          |


| **JULY**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| ?? (------)           | Last day to accept or withdraw food/accommodation bursary            |
| ?? (------)           | Last day to register with guaranteed swag. Registrations after this date are still possible, but swag is not guaranteed. |


| **AUGUST**            |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| *DebCamp*             |                                                                      |
| 16 (Sunday)           | First day of DebCamp                                                 |
| 17 (Monday)           | Second day of DebCamp                                                |
| 18 (Tuesday)          | Third day of DebCamp                                                 |
| 19 (Wednesday)        | Fourth day of DebCamp                                                |
| 20 (Thursday)         | Fifth day of DebCamp                                                 |
| 21 (Friday)           | Sixth day of DebCamp                                                 |
| 22 (Saturday)         | Seventh day of DebCamp. Arrival day for DebConf                      |
| *DebConf*             |                                                                      |
| 23 (Sunday)           | First day of DebConf / opening ceremony                              |
| 24 (Monday)           | Second day of DebConf / women and diversity lunch  (tbc.) / evening: cheese and wine party  (tbc.)  |
| 25 (Tuesday)          | Third day of DebConf                                                 |
| 26 (Wednesday)        | Fourth day of DebConf / all day: day trip  (tbc.)                    |
| 27 (Thursday)         | Fifth day of DebConf / evening: conference dinner  (tbc.)            |
| 28 (Friday)           | Sixth day of DebConf                                                 |
| 29 (Saturday)         | Last day of DebConf / closing ceremony / teardown                    |
| 30 (Sunday)           | Departure day :-(                                                    |
