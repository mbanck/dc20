# -*- coding: utf-8 -*-
from collections import namedtuple

from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import engines

from register.models import Accomm, Attendee, Food


SUBJECT = "Welcome to DebConf20! (arrival details)"

TEMPLATE = """\
Dear {{ name }},

Welcome to DebConf20, hosted at the Central Campus of UTFPR in Curitiba, Brazil.
We're delighted that you decided to join us for what we hope will be one of the
best DebConfs yet.

Please read the contents of this mail and the Conference Venue page [1] in the
web site, carefully.  They contain important information about your travel to
Haifa, and DebConf20.

[1] https://debconf20.debconf.org/about/venue/

== Before departing for Haifa ==

Print this e-mail and the relevant information in the web site (e.g. a venue
map).

Check your data, and report any errors and changes (including delays or
cancellation) as soon as possible to <registration@debconf.org>:

Arriving: {{ attendee.arrival|date:"DATETIME_FORMAT" }}
Departing: {{ attendee.departure|date:"DATETIME_FORMAT" }}{% if food %}
Meals requested:{% for day in meals %}
{{ day.date|date:"SHORT_DATE_FORMAT" }}: {{ day.meals|join:", "|title }}{% endfor %}
Diet: {{ food.diet|default:"Whatever is provided" }}{% if food.special_diet %}
Details: {{ food.special_diet }}{% endif %}{% else %}
No conference-organised food requested.{% endif %}

Familiarize yourself with the Codes of Conduct for both Debian [2] and DebConf
[3].  If you are harassed, or observe harassment, you can contact
<antiharassment@debian.org> or members of the local anti-harassment team who
are recognizable by their conference badges.  For any other kind of problem,
the Front Desk and Organisation team may be of help.

[2] http://debconf.org/codeofconduct.shtml
[3] https://www.debian.org/code_of_conduct

Check your travel documents.  Will your passport remain valid for the duration
of your stay?  Do you have all necessary travel authorizations (such as a
visa)?

Notify your bank that you'll be travelling to Brazil to avoid having your
card(s) blocked.

FIXME:

Do you have travel insurance? Although the Israeli public health insurance
system covers travellers, this could be important should you need medical
attention.  Be sure to also check with your insurance if you're covered for lost
or stolen items, such as electronics.

Cheese & Wine allowance:
Up to 5 kg of food may be imported (but only up to 1 kg of each type of food) per person.
Up to 2L of wine and 1L of other drinks per person.

== Your Accommodation Details ==
{% if not accomm %}
FIXME
You don't have any conference-provided accommodation.  Remember to print the
details of your self-organised accommodation in case you're asked for proof by
immigration officials.{% else %}

=== Haifa University Dormitories ===

According to our records, you're registered to stay in the dormitories on the
following days:

{% for stay in stays %}
Check-in: {{ stay.0|date }}
Checkout: {{ stay.1|date }}{% endfor %}
Room: {{ accomm.room }}{% if roommates %}
Roommates: {{roommates|join:", "}}{% endif %}

The check-in will be done at the hotel front desk, you just need to go there
and give your name.  They will know which room is assigned to you.

FIXME

The Nacional Inn provides everything a regular hotel provides (bed sheets,
blanket, pillow, towels, shampoo, soap, and so on).  The rooms contain an air
conditioner, TV, minibar and hair dryer in the bathroom.  The common area of
the hotel has a gym, a swimming pool and a sauna.

FIXME:

You will need to bring personal toiletaries, such as toothbrush and toothpaste.
There is a laundry third-party service available to the hotel guests.
You'll be sharing a room with others, so if you're sensitive to noise when
sleeping, consider bringing earplugs.

https://debconf19.debconf.org/about/accommodation/{% endif %}

== What to Bring ==

FIXME:

An international adapter (if required) and power supplies for all your devices.
Brazil uses a socket similar to the Swiss, compatible with the Europlug.
In most of Curitiba the voltage is 127V.

Warmer clothes.  It's winter in Curitiba, but it is Brazilian winter which is
usually cold in the begining and end of the day, and warmer during the day.
The temperature should range from 5 to 25°C.  This is a wide temperature range,
so be prepared for it!  It may rain, so a raincoat and/or umbrella will be
useful.  Don't forget a reusable water bootle and sunscreen if you plan to
enjoy the outdoors.

An Ethernet cable is always useful to have, if we have WiFi trouble.  WiFi
should be available in every area we use on the campus and in the hotel.

Printed PGP/GPG fingerprint slips, if you want to have your key signed by other
peers.

== Airport Arrival ==
FIXME:

=== Public Transit (E32 bus) ===

This is the cheapest option (R$ 4.50, about 1 USD).  The airport bus (line E32,
grey), runs from the Airport to the Terminal Boqueirão.  Transfer (for free)
onto the Bi-articulado Boqueirão to the UTFPR bus station, outside the campus.
The entrance is on Av. Sete de Setembro, just around the corner.

=== Coach ===

The cost is R$ 15 (about 4 USD).  There are signals throughout the airport. It
is a good idea you say to the driver you want to get off at the Shopping
Estação bus stop. The campus is one block west of the shopping centre.

=== Taxis ===

The cost is around R$ 70 (about 17 USD).  There is only one company and the
cars are blue and white, you will see them right after the airport exit door.

=== Apps ===

If you are OK with using proprietary car ride apps, you can use Uber or 99POP
(99 works with particular car or taxi).  Apparently, 99POP is only available in
the Brazilian app stores for Android and IOS, so do not worry if you cannot
find them. With those apps you can save some money when comparing to regular
taxis (about 35 BRL).

== Front Desk location ==

The Front Desk is located in the main hall between the restaurant and the
mini-auditorium.  Look for a glass-walled room with DebConf signs.  If you're
arriving late (after 19:00) and Front Desk is closed, go directly to wherever
your accomodation is.  If you are staying in the hotel Nacional Inn there will
always have some one in its front desk to help you.

If you run into any problems, you can call the DebConf Front Desk at
+55 41 99791-3565.  Save this in your phone now :)

Have a safe trip.  We look forward to seeing you in Curitiba!

The DebConf Team
"""

Stay = namedtuple('Stay', ('checkin', 'checkout'))
Meal = namedtuple('Meal', ('date', 'meals'))


def meal_sort(meal):
    return ['breakfast', 'lunch', 'dinner'].index(meal)


class Command(BaseCommand):
    help = 'Send welcome emails'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Send mail to a specific user, only'),

    def badger(self, attendee, dry_run):
        name = attendee.user.userprofile.display_name()
        to = attendee.user.email

        try:
            accomm = attendee.accomm
            roommates = accomm.get_roommates()
            if roommates:
                roommates = [
                    '%s (%s)' % (attendee.user.userprofile.display_name(), attendee.user.username)
                    for attendee
                    in roommates
                ]
        except Accomm.DoesNotExist:
            accomm = None
            roommates = None

        if accomm and accomm.attendee.user.bursary.accommodation_status != 'accepted':
            accomm = None
            roommates = None

        try:
            food = attendee.food
        except Food.DoesNotExist:
            food = None

        if accomm:
            stays = list(attendee.accomm.get_stay_details())
        else:
            stays = None

        meals = []
        if food:
            by_day = {}
            for meal in food.meals.all():
                by_day.setdefault(meal.date, []).append(meal.meal)
            for date, days_meals in sorted(by_day.items()):
                meals.append(Meal(date, sorted(days_meals, key=meal_sort)))

        ctx = {
            'accomm': accomm,
            'roommates': roommates,
            'attendee': attendee,
            'food': food,
            'meals': meals,
            'name': name,
            'stays': stays,
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        queryset = Attendee.objects.all()
        if options['username']:
            queryset = queryset.filter(user__username=options['username'])

        for attendee in queryset:
            self.badger(attendee, dry_run)
